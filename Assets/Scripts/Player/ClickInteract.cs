﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickInteract : MonoBehaviour
{

    public LayerMask clickMask;
    public float clickDistance = 5;

    public Color clickable, notClickable;
    private RaycastHit rayHit;

    void Update()
    {
        UIManager.Instance.crosshairImage.color = notClickable;
        if (Physics.Raycast(transform.position, transform.forward, out rayHit, clickDistance, clickMask))
        {
            if (rayHit.transform.GetComponent<IClickable>() != null)
            {
                UIManager.Instance.crosshairImage.color = clickable;
                if(Input.GetButtonDown("Fire1")) rayHit.transform.GetComponent<IClickable>().ClickAction();
            }
        }          
    }
}
