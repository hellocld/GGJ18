﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float _moveSpeed = 3;
    public AudioClip[] footsteps;
    public float footstepGap = 0.5f;

    private CharacterController _characterController;
    private AudioSource _audioSource;
    private float footstepTimer = 0;

    void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!_characterController.isGrounded)
        {
            _characterController.Move(Vector3.down * 9.81f * Time.deltaTime);
        }

        _characterController.Move(transform.forward * Input.GetAxis("Vertical") *_moveSpeed);
        _characterController.Move(transform.right*Input.GetAxis("Horizontal") * _moveSpeed);

        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            footstepTimer += Time.deltaTime;
            if (footstepTimer > footstepGap)
            {
                _audioSource.PlayOneShot(footsteps[Random.Range(0, footsteps.Length)]);
                footstepTimer = 0;
            }
        }
    }

}
