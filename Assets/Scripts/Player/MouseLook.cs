﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float _lookSpeed;
    public Transform _camera;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        transform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * _lookSpeed * Time.deltaTime);

        _camera.Rotate(Vector3.right, -Input.GetAxis("Mouse Y") * _lookSpeed * Time.deltaTime);    
    }
}
