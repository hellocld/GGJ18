﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using Button = UnityEngine.UI.Button;

public class MainMenuManager : MonoBehaviour
{

    public Button playDayButton, playNightButton, creditsButton, quitButton, closeCreditsButton;
    public GameObject menuPanel, creditsPanel;


    void Start()
    {
        playDayButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Day Map");
        });

        playNightButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Night Map");
        });

        creditsButton.onClick.AddListener(() =>
        {
            menuPanel.SetActive(false);
            creditsPanel.SetActive(true);
        });

        quitButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });

        closeCreditsButton.onClick.AddListener(() =>
        {
            menuPanel.SetActive(true);
            creditsPanel.SetActive(false);
        });

        menuPanel.SetActive(true);
        creditsPanel.SetActive(false);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

}
