﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloorTileTrigger : MonoBehaviour
{

    public UnityEvent triggerEvent;


    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Trigger");
        if (col.gameObject.CompareTag("Player"))
        {
            triggerEvent.Invoke();
        }
    }

}
