﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayer : MonoBehaviour
{

    public Transform teleportLocation;

    public void TriggerTeleport()
    {
        GameManager.Instance.playerObject.transform.position = teleportLocation.position;
    }

}
