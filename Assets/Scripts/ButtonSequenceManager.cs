﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonSequenceManager : MonoBehaviour
{
    public int[] sequence;

    public UnityEvent successEvent;

    private Queue<int> inputBuffer;

    void Start()
    {
        inputBuffer = new Queue<int>(sequence.Length);
    }

    public void ButtonEntry(int id)
    {
        inputBuffer.Enqueue(id);
        if (inputBuffer.Count > sequence.Length)
        {
            inputBuffer.Dequeue();
        }

        string queueLog = "inputBuffer = ";
        for (int i = 0; i < inputBuffer.Count; i++)
        {
            queueLog += inputBuffer.ToArray()[i] + ", ";
        }

        Debug.Log(queueLog);


        // Compare queue to sequence
        if (inputBuffer.Count < sequence.Length) return;

        for (int i = 0; i < sequence.Length; i++)
        {
            if (inputBuffer.ToArray()[i] != sequence[i]) return;
        }

        successEvent.Invoke();
    }

}
