﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{

    public int maxPentalties = 5;
    public int currentPenalties;

    public GameObject playerObject;
    public void ApplyPenalty()
    {
        currentPenalties++;
        UIManager.Instance.AddStrike();
        if (currentPenalties >= maxPentalties)
        {
            GameOver();
        }
        
    }

    private void GameOver()
    {
        Debug.Log("Game Over.");
    }

    public void WinScreen()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        playerObject.GetComponent<PlayerController>().enabled = false;
        playerObject.GetComponent<MouseLook>().enabled = false;
        UIManager.instance.EnableWinScreen();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Main Menu");
        }
    }

}
