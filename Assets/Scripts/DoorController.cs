﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

    public Transform openPosition, closePosition;
    public float openTime;

    public bool opening = false;

    public void OpenDoor()
    {
        if(!opening) StartCoroutine(OpenCoroutine());
    }

    IEnumerator OpenCoroutine()
    {
        opening = true;
        
        float openTimer = 0;
        while (openTimer < openTime)
        {
            openTimer += Time.deltaTime;
            transform.position = Vector3.Lerp(closePosition.position, openPosition.position, openTimer / openTime);
            yield return null;
        }
    }


}
