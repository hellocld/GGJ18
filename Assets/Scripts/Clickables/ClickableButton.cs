﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClickableButton : MonoBehaviour, IClickable
{
    public UnityEvent ClickUnityAction;
    
    public void ClickAction()
    {
        if (ClickUnityAction != null) ClickUnityAction.Invoke();
    }
}
