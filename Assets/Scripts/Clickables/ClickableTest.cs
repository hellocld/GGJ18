﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableTest : MonoBehaviour, IClickable {
    public void ClickAction()
    {
        Debug.LogFormat("You clicked {0}", gameObject.name);
    }
}
