﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public Image crosshairImage;

    public GameObject strikePanel;
    public GameObject strikeObject;
    public GameObject winPanel;

    public Button quitButton;

    void Start()
    {
        winPanel.SetActive(false);

        quitButton.onClick.AddListener(()=>
        {
            Debug.Log("Quitting");
            SceneManager.LoadScene("Main Menu");
        });
    }

    public void AddStrike()
    {
        Instantiate(strikeObject, strikePanel.transform);
    }

    public void EnableWinScreen()
    {
        winPanel.SetActive(true);
        crosshairImage.gameObject.SetActive(false);
    }
}
